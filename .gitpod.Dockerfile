# Copyright (c) 2021 Tailscale Inc & AUTHORS All rights reserved.
# Copyright (c) 2021-present Andrei Jiroh Halili
# Use of this source code is governed by a BSD-style license that can be found
# in the LICENSE file.
FROM quay.io/gitpodified-workspace-images/full

# Copy the service file and other overlay files to / as root
USER root
COPY docker/overlay/ /
# Ensure ~/.bashrc.d/20-passwdstore.bashrc is owned by correct user.
RUN chown gitpod:gitpod /home/gitpod/.bashrc.d/20-passwdstore.bashrc \
    # hint: maintained by WireGuard creator and part of zx2c4 family of projects
    && instal-packages pass \
    # generally upgrade deb packages to keep things fresh
    && upgrade-packages

USER gitpod
# Nice wrapper above pass for TOTP part, where in cases you can't use the Bitwarden CLI
# Repo: https://github.com/WhyNotHugo/totp-cli/
RUN pip3 install --no-cache-dir totp
