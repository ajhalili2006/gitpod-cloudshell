# Andrei Jiroh's Cloud Dev Environment Setup With Gitpod + Tailscale

This repository contains:

* an Gitpod workspace configuration with custom Dockerfile, using `quay.io/gitpodified-workspace-images/full` as base image
* some files in `overlay` to arrange rootfs a bit, withoutadding multiple lines of `RUN mkdir ...` and `COPY` in `.gitpod.Dockerfile`
*some direnv magic through `.envrc` within the cloned repository inside Gitpod

The README is a bit WIP for now, but I will expand it soon as possible.

## Prerequsites

* [Reusable authkey for Tailscale](https://login.tailscale.com/admin/settings/authkeys) as `TAILSCALE_AUTHKEY` variable.
* GPG keys for PasswordStore and commit signing
* Doppler CLI API Key, we'll cover it later.
