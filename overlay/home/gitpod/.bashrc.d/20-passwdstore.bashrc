#!/usr/bin/env bash
# shellcheck shell=bash

# If we're inside Gitpod, make sure to point pass ad gpg to presistent paths.
if [[ $GITPOD_REPO_ROOT != "" ]]; then
    export PASSWORD_STORE_DIR="$GITPOD_REPO_ROOT/.passwdstore" GNUPGHOME="$GITPOD_REPO_ROOT/.gpg" 
fi

# Theoretically, we assume you have your own GPG keys for passwdstore to use,
# preferrly that special key for passwordsstore use only. The manpage says that
# you can also use multiple keys as long as they're separated by spaces.
# For the reference to the misspelled versions of Gildedguy's name, see https://rtapp.tk/gildedguy-name-spelling
# and https://andreijiroh.rtapp.tk/guys-i-am-not-mikedmoy. This commented export is there For
# documentation purposes only. You should set this through `gp env`.
#export PASSWORD_STORE_KEY="ajhalili20006+not-guildedguy@gmail.com"
